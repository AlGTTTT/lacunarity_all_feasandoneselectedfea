
#define _CRT_SECURE_NO_DEPRECATE // 1
#define _CRT_NONSTDC_NO_DEPRECATE // 1

#include "Lacunarity_All_FeasAndOneSelectedFea_function.cpp"

int main()
{
	int doLacunarityOf_ColorImage(

		const Image& image_in,
		COLOR_IMAGE *sColor_Image,

		float fOneDim_Lacunar_Arrf[], //[nNumOfFeasForLacunar_AtIntensity_Range] = [nNumOfLenOfSquareMax*3 + nNumOfLenOfSquareMax*nNumOfSquareOccurrence_Intervals]
		float &fFractal_Dimension);

	int doOne_Fea_OfLacunarityOf_ColorImage(
		const int nNumOfSelectedLacunarityFeaf, //< nNumOfLacunarFeasFor_OneDimTot

		const Image& image_in,

		const COLOR_IMAGE *sColor_Imagef,
		//float fOneDim_Lacunar_Arrf[], //[nNumOfLacunarFeasFor_OneDimTot] //[nNumOfFeasForLacunar_AtIntensity_Range] = [nNumOfLenOfSquareMax*3 + nNumOfLenOfSquareMax*nNumOfSquareOccurrence_Intervals]
		float &fOne_SelectedLacunar_Feaf); //[nNumOfLacunarFeasFor_OneDimTot] //[nNumOfFeasForLacunar_AtIntensity_Range] = [nNumOfLenOfSquareMax*3 + nNumOfLenOfSquareMax*nNumOfSquareOccurrence_Intervals]

	int
		iFeaf,
		nRes;

	float
		fOne_SelectedLacunar_Fea,

		fOneDim_Lacunar_Arr[nNumOfLacunarFeasFor_OneDimTot], //[nNumOfFeasForLacunar_AtIntensity_Range] = [nNumOfLenOfSquareMax*3 + nNumOfLenOfSquareMax*nNumOfSquareOccurrence_Intervals]
		fFractal_Dimension_Out;

//	float
	//	fOneDim_Lacunar_Arr[nNumOfLacunarFeasFor_OneDimTot]; //[nNumOfLacunarFeasFor_OneDimTot]
		////////////////////////////////////////////////////////////////////////////////////////////////
/*
	fout_lr = fopen("wMain_RemovingLabels.txt", "w");

	if (fout_lr == NULL)
	{
		printf("\n\n fout_lr == NULL");
		getchar(); exit(1);
	} //if (fout_lr == NULL)
*/
	Image image_in;

	image_in.read("evans spleen1.bmp");
		//image_in.read("output.png");
	//image_in.read("Row A Day 1.png");
	//image_in.read("Row A Day 3.png");

	//image_in.read("Row B Day 1.png");
	//image_in.read("Row B Day 3.png");

	//image_in.read("Row C Day 1.png");
	//image_in.read("Row C Day 3.png");

	Image image_out;
	COLOR_IMAGE sColor_Image; //

	nRes = doLacunarityOf_ColorImage(
				image_in, // Image& image_in,
		&sColor_Image, //COLOR_IMAGE *sColor_Image,

		fOneDim_Lacunar_Arr, //[nNumOfLacunarFeasFor_OneDimTot], //[nNumOfFeasForLacunar_AtIntensity_Range] = [nNumOfLenOfSquareMax*3 + nNumOfLenOfSquareMax*nNumOfSquareOccurrence_Intervals]
		fFractal_Dimension_Out); // 

	if (nRes == UNSUCCESSFUL_RETURN) // -1
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n The image has not been processed properly.");

		fprintf(fout_lr,"\n\n The image has not been processed properly.");

		fflush(fout_lr); //debugging;
		printf("\n\n The image has not been processed properly: please press any key to exit");	getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN; //
	} // if (nRes == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n After 'doLacunarityOf_ColorImage': fFractal_Dimension_Out = %E, nNumOfLacunarFeasFor_OneDimTot = %d", fFractal_Dimension_Out, nNumOfLacunarFeasFor_OneDimTot);
	fprintf(fout_lr, "\n\n After 'doLacunarityOf_ColorImage': fFractal_Dimension_Out = %E, nNumOfLacunarFeasFor_OneDimTot = %d", fFractal_Dimension_Out, nNumOfLacunarFeasFor_OneDimTot);

	for (iFeaf = 0; iFeaf < nNumOfLacunarFeasFor_OneDimTot; iFeaf++)
	{
		fprintf(fout_lr, "\n fOneDim_Lacunar_Arr[%d] = %E", iFeaf, fOneDim_Lacunar_Arr[iFeaf]);
	}//for (iFeaf = 0; iFeaf < nNumOfLacunarFeasFor_OneDimTot; iFeaf++)

	fflush(fout_lr); //debugging;
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
//////////////////////////////////////////////

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n Before 'doOne_Fea_OfLacunarityOf_ColorImage': nNumOfSelectedLacunarityFea = %d, nNumOfLacunarFeasFor_OneDimTot = %d, nNumOfIntensity_IntervalsForLacunarTot = %d",
		 nNumOfSelectedLacunarityFea, nNumOfLacunarFeasFor_OneDimTot, nNumOfIntensity_IntervalsForLacunarTot);

	fprintf(fout_lr, "\n\n Before 'doLacunarityOf_ColorImage':   nNumOfSelectedLacunarityFea = %d, nNumOfLacunarFeasFor_OneDimTot = %d, nNumOfIntensity_IntervalsForLacunarTot = %d",
		nNumOfSelectedLacunarityFea, nNumOfLacunarFeasFor_OneDimTot, nNumOfIntensity_IntervalsForLacunarTot);
	
	fflush(fout_lr); //debugging;
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	nRes = doOne_Fea_OfLacunarityOf_ColorImage(
		nNumOfSelectedLacunarityFea, //const int nNumOfSelectedLacunarityFeaf, //< nNumOfLacunarFeasFor_OneDimTot

		image_in, //const Image& image_in,

		&sColor_Image, //const COLOR_IMAGE *sColor_Imagef,

		//float fOneDim_Lacunar_Arrf[], //[nNumOfLacunarFeasFor_OneDimTot] //[nNumOfFeasForLacunar_AtIntensity_Range] = [nNumOfLenOfSquareMax*3 + nNumOfLenOfSquareMax*nNumOfSquareOccurrence_Intervals]
		fOne_SelectedLacunar_Fea); // float &fOne_SelectedLacunar_Feaf); //[nNumOfLacunarFeasFor_OneDimTot] //[nNumOfFeasForLacunar_AtIntensity_Range] = [nNumOfLenOfSquareMax*3 + nNumOfLenOfSquareMax*nNumOfSquareOccurrence_Intervals]

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n After 'doOne_Fea_OfLacunarityOf_ColorImage': fOne_SelectedLacunar_Fea = %E, nNumOfSelectedLacunarityFea = %d, fOneDim_Lacunar_Arr[%d] = %E", 
		fOne_SelectedLacunar_Fea, nNumOfSelectedLacunarityFea, nNumOfSelectedLacunarityFea,fOneDim_Lacunar_Arr[nNumOfSelectedLacunarityFea]);

	fprintf(fout_lr, "\n\n After 'doLacunarityOf_ColorImage':  fOne_SelectedLacunar_Fea = %E, nNumOfSelectedLacunarityFea = %d, fOneDim_Lacunar_Arr[%d] = %E",
		fOne_SelectedLacunar_Fea, nNumOfSelectedLacunarityFea, nNumOfSelectedLacunarityFea, fOneDim_Lacunar_Arr[nNumOfSelectedLacunarityFea]);
		
	fflush(fout_lr); //debugging;
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
///////////////////////////////////////////
	//image_out.write("01-144_LCC_mask_NoLabels.png");

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n Please press any key to exit"); getchar(); //exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	return SUCCESSFUL_RETURN;
} //int main()

//printf("\n\n Please press any key:"); getchar();