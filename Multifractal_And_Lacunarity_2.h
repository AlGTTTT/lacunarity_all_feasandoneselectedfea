#define _CRT_SECURE_NO_DEPRECATE // 1
#define _CRT_NONSTDC_NO_DEPRECATE // 1

//using namespace imago;

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
//#include <iostream.h>
#include <string.h>
//#include <iomanip.h>
//#include <fstream.h>
#include <assert.h>

#include "image.h"
//#include "median_filter.h"

#define SUCCESSFUL_RETURN 0 //2 and (-2) are also normal returns
#define UNSUCCESSFUL_RETURN (-1)

#define SQRT2 1.414213562
#define BLACK 0
#define WHITE 1

#define MASK_OUTPUT
	//#define COMMENT_OUT_ALL_PRINTS

#define nLenMax_La 6000 
#define nWidMax_La 6000 

#define nLenMin_La 50 
#define nWidMin_La 50

//#define nImageSizeMax (nLenMax_La*nWidMax_La)

///////////////////////////////////////////////////////////////////
#define nLarge 1000000
#define fLarge 1.0E+10 //6

#define feps 0.000001
//#define pi 3.14159
#define PI 3.1415926535
//////////////////////////////////////////////////////
//the multifractal header
//Eugen Mircea Anitas "Small-angle scattering (neutrons, X-rays, Light) from complex systems. Fractal and multifractal models for interpretation of experimental data".
//Springer briefs in physics, 2018

#define MASK_OUTPUT
	//#define COMMENT_OUT_ALL_PRINTS

#define nLenMax 6000
#define nWidMax 6000

#define nLenMin 50
#define nWidMin 50

#define nImageSizeMax (nLenMax*nWidMax)

//////////////////////////////////////////////////////
#define nLenSubimageToPrintMin  0 //342
#define nLenSubimageToPrintMax  4000  //2500

#define nWidSubimageToPrintMin 0 //185 //3320 //40 //5410 //370 //2470

#define nOneWidToPrint  226 //56 //371 //2476 //
#define nWidSubimageToPrintMax 300 //250 //3390 //5470 //2670 //470 //3970

//#define nWidSubimageToPrintMin 9 //10 //3712 //1980 //499 //229 //415 //478 //191 //344 //193 //416 //814
//#define nWidSubimageToPrintMax 9 //3714 //502 //416 //192 //361 //195 //240 //420
///////////////////////////////////////////////////////////////////////
#define nIntensityStatMin 10 //85
#define nIntensityStatMinForEqualization 0 //85

#define nIntensityStatForReadMax 65535

#define nIntensityStatBlack 0 //85
#define nIntensityStatMax 255

#define nIntensityStatWhite (nIntensityStatMax)
/////////////////////////////////////////////////////////////

#define nNumOfIters_ForDim_2powerN_Max 14

//#define nThresholdForIntensitiesMin 20 //5 //1 //110
#define nThresholdForIntensitiesMin 70 //80 // all colors
//#define nThresholdForIntensitiesMin 100
//#define nThresholdForIntensitiesMin 150

#define nThresholdForIntensitiesMax 255

#define fWeightOfRed_InStr 1.0 // 0.0
#define fWeightOfGreen_InStr 1.0
#define fWeightOfBlue_InStr 1.0 //0.0 //

#define nNumOfSquareOccurrence_Intervals 4

#define nLenOfSquareMin 2
#define nNumOfNonZero_ObjectSquaresTotMin 1

#define nNumOf_Qs_Tot 21 //20 // including 0 -- 21
#define nQs_Min (-10)
#define nQs_Max 10

//#define nWidthOfQ_Step ( (nQs_Max - nQs_Min)/(nNumOf_Qs_Tot - 1) )
#define nNumOfIntensity_IntervalsForMultifractal 8 // 2^n

#define nNumOfIntensity_IntervalsForMultifractalTot ( ((nNumOfIntensity_IntervalsForMultifractal + 1)*nNumOfIntensity_IntervalsForMultifractal)/2)

#define nNumOfFeasForMultifractalTot (nNumOf_Qs_Tot*nNumOfIntensity_IntervalsForMultifractalTot)

#define nNumOfMultifractalFeasFor_OneDimTot (3*nNumOfFeasForMultifractalTot)

//#define nNumOfSelectedFea 1
//#define nNumOfSelectedFea 1100 // >= nNumOfFeasForMultifractalTot and < 2*nNumOfFeasForMultifractalTot
//#define nNumOfSelectedFea 1102 // >= nNumOfFeasForMultifractalTot and < 2*nNumOfFeasForMultifractalTot
#define nNumOfSelectedFea 2247 // >= nNumOfFeasForMultifractalTot and < 2*nNumOfFeasForMultifractalTot
#define nNumOfSelected_SpectrumFea (nNumOfSelectedFea - nNumOfFeasForMultifractalTot)

//////////////////////////////////////////////////////

//#define nLenSubimageToPrintMin  0 //342 
//#define nLenSubimageToPrintMax  4000  //2500

//#define nWidSubimageToPrintMin 0 //185 //3320 //40 //5410 //370 //2470 

//#define nOneWidToPrint  226 //56 //371 //2476 //
//#define nWidSubimageToPrintMax 300 //250 //3390 //5470 //2670 //470 //3970 

///////////////////////////////////////////////////////////////////////
//the lacunarity header
#define nIntensityStatForReadMax_La 65535

//#define nIntensityStatBlack 0 //85 
#define nIntensityStatMax_La 255 

//#define nIntensityStatWhite (nIntensityStatMax_La)
/////////////////////////////////////////////////////////////

#define nNumOfIters_ForDim_2powerN_Max_La 14

//#define nThresholdForIntensitiesMin 70 //80 // all colors
//#define nThresholdForIntensitiesMax 255

#define nThresholdForIntensities_ForFractalDimMin_La 70 //80 // all colors
#define nThresholdForIntensities_ForFractalDimMax_La 255

#define fWeightOfRed_InStr_La 1.0 // 0.0
#define fWeightOfGreen_InStr_La 1.0
#define fWeightOfBlue_InStr_La 1.0 //0.0 //

#define nNumOfSquareOccurrence_Intervals_La 4 // = 2^n
#define nDivisorForLenOfSquareMax_Init_La (nNumOfSquareOccurrence_Intervals_La/2) // initially, nLenOfSquareCurf = nDim_2pNf/ nDivisorForLenOfSquareMax_Init_La;
//////////////////////////////////////////////////////////

#define nLenOfSquareMin_La 2
///////////////////////////////////////////////

//#define nNumOfLenOfSquareMax_La 7 //instead of 8, starting from the 4 squares per the length
//#define nNumOfLenOfSquareMax_La 8 //instead of 8, starting from the 4 squares per the length
//#define nNumOfLenOfSquareMax_La 9 //instead of 8, starting from the 4 squares per the length
#define nNumOfLenOfSquareMax_La 10 //instead of 8, starting from the 4 squares per the length

//#define nDim_2pN_Max_La 1024 //512 // == 2^(nNumOfLenOfSquareMax_La + 2)
//#define nDim_2pN_Max_La 2048 //512 // == 2^(nNumOfLenOfSquareMax_La + 2)
#define nDim_2pN_Max_La 4096 //512 // == 2^(nNumOfLenOfSquareMax_La + 2)
//#define nDim_2pN_Max_La  (2^(nNumOfLenOfSquareMax_La + 2))

//////////////////////////////////////
#define nNumOfFeasForLacunar_AtIntensity_Range (nNumOfLenOfSquareMax_La*3 + (nNumOfLenOfSquareMax_La*nNumOfSquareOccurrence_Intervals_La) )

#define nNumOfIntensity_IntervalsForLacunar 8 // 2^n

#define nNumOfIntensity_IntervalsForLacunarTot ( ((nNumOfIntensity_IntervalsForLacunar + 1)*nNumOfIntensity_IntervalsForLacunar)/2)

#define nNumOfLacunarFeasFor_OneDimTot (nNumOfFeasForLacunar_AtIntensity_Range*nNumOfIntensity_IntervalsForLacunarTot)

////////////////////////
#define nNumOfNonZero_ObjectSquaresTotMin_La 1

#define nNumOfIntensity_IntervalsForLacunar 8 // 2^n

#define nNumOfIntensity_IntervalsForLacunarTot ( ((nNumOfIntensity_IntervalsForLacunar + 1)*nNumOfIntensity_IntervalsForLacunar)/2)

#define nNumOfFeasForLacunarTot (nNumOf_Qs_Tot*nNumOfIntensity_IntervalsForLacunarTot)

#define nNumOfSelectedLacunarityFea 1
///////////////////////////////////////////////////////
typedef struct
{
	float 
		fWeightOfRed; //<= 1.0
	float
		fWeightOfGreen; //<= 1.0
	float
		fWeightOfBlue; //<= 1.0

} WEIGHTES_OF_RGB_COLORS;

/////////////////////////////////////////////////////////

typedef struct
{
	int nSideOfObjectLocation; //-1 - left, 1 - right

	int nIntensityOfBackground_Red; //
	int nIntensityOfBackground_Green; //
	int nIntensityOfBackground_Blue; //

	int nWidth;
	int nLength;

	int *nLenObjectBoundary_Arr;

	int *nRed_Arr;
	int *nGreen_Arr;
	int *nBlue_Arr;

	int *nIsAPixelBackground_Arr;

} COLOR_IMAGE;

////////////////////////////////////////////////////////////////
typedef struct
{

	int nWidth;
	int nLength;

	int *nEmbeddedImage_Arr; //0-object, 1-background

} EMBEDDED_IMAGE_BLACK_WHITE;

/*

//Eugen Mircea Anitas "Small-angle scattering (neutrons, X-rays, Light) from complex systems. Fractal and multifractal models for interpretation of experimental data".
//Springer briefs in physics, 2018

#define MASK_OUTPUT
	//#define COMMENT_OUT_ALL_PRINTS

#define nLenMax 6000
#define nWidMax 6000

#define nLenMin 50
#define nWidMin 50

#define nImageSizeMax (nLenMax*nWidMax)

//////////////////////////////////////////////////////
#define nLenSubimageToPrintMin  0 //342
#define nLenSubimageToPrintMax  4000  //2500

#define nWidSubimageToPrintMin 0 //185 //3320 //40 //5410 //370 //2470

#define nOneWidToPrint  226 //56 //371 //2476 //
#define nWidSubimageToPrintMax 300 //250 //3390 //5470 //2670 //470 //3970

//#define nWidSubimageToPrintMin 9 //10 //3712 //1980 //499 //229 //415 //478 //191 //344 //193 //416 //814
//#define nWidSubimageToPrintMax 9 //3714 //502 //416 //192 //361 //195 //240 //420
///////////////////////////////////////////////////////////////////////
#define nIntensityStatMin 10 //85
#define nIntensityStatMinForEqualization 0 //85

#define nIntensityStatForReadMax 65535

#define nIntensityStatBlack 0 //85
#define nIntensityStatMax 255

#define nIntensityStatWhite (nIntensityStatMax)
/////////////////////////////////////////////////////////////

#define nNumOfIters_ForDim_2powerN_Max 14

//#define nThresholdForIntensitiesMin 20 //5 //1 //110
#define nThresholdForIntensitiesMin 70 //80 // all colors
//#define nThresholdForIntensitiesMin 100
//#define nThresholdForIntensitiesMin 150

#define nThresholdForIntensitiesMax 255

#define fWeightOfRed_InStr 1.0 // 0.0
#define fWeightOfGreen_InStr 1.0
#define fWeightOfBlue_InStr 1.0 //0.0 //

#define nNumOfSquareOccurrence_Intervals 4

#define nLenOfSquareMin 2
#define nNumOfNonZero_ObjectSquaresTotMin 1

#define nNumOf_Qs_Tot 21 //20 // including 0 -- 21
#define nQs_Min (-10)
#define nQs_Max 10

//#define nWidthOfQ_Step ( (nQs_Max - nQs_Min)/(nNumOf_Qs_Tot - 1) )
#define nNumOfIntensity_IntervalsForMultifractal 8 // 2^n

#define nNumOfIntensity_IntervalsForMultifractalTot ( ((nNumOfIntensity_IntervalsForMultifractal + 1)*nNumOfIntensity_IntervalsForMultifractal)/2)

#define nNumOfFeasForMultifractalTot (nNumOf_Qs_Tot*nNumOfIntensity_IntervalsForMultifractalTot)

#define nNumOfMultifractalFeasFor_OneDimTot (3*nNumOfFeasForMultifractalTot)

//#define nNumOfSelectedFea 1
//#define nNumOfSelectedFea 1100 // >= nNumOfFeasForMultifractalTot and < 2*nNumOfFeasForMultifractalTot
//#define nNumOfSelectedFea 1102 // >= nNumOfFeasForMultifractalTot and < 2*nNumOfFeasForMultifractalTot
#define nNumOfSelectedFea 2247 // >= nNumOfFeasForMultifractalTot and < 2*nNumOfFeasForMultifractalTot
#define nNumOfSelected_SpectrumFea (nNumOfSelectedFea - nNumOfFeasForMultifractalTot)

*/


